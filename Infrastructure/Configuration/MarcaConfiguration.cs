using Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Configuration;

public class MarcaConfiguration : IEntityTypeConfiguration<Marca>
{
    public void Configure(EntityTypeBuilder<Marca> builder)
    {
        builder.ToTable("MARCA");

        builder.Property(c =>c.Id).IsRequired();

        builder.Property(e => e.Nombre)
                .HasMaxLength(100);
    }    
}
