﻿using System;
using System.Collections.Generic; 

namespace Core.Entities;

public class Categoria: BaseEntity
{
    public Categoria()
    {
        Productos = new HashSet<Producto>();
    }

    public string Nombre { get; set; } = null!;

    public virtual ICollection<Producto> Productos { get; set; }
}

