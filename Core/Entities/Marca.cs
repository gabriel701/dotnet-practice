﻿using System;
using System.Collections.Generic;

namespace Core.Entities;

public class Marca:BaseEntity
{
    public Marca()
    {
        Productos = new HashSet<Producto>();
    }

    public string Nombre { get; set; } = null!;

    public virtual ICollection<Producto> Productos { get; set; }
}

